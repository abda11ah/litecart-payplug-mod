<?php

document::$snippets['title'][] = language::translate('index:head_title', 'One fancy web shop');
document::$snippets['keywords'] = language::translate('index:meta_keywords', '');
document::$snippets['description'] = language::translate('index:meta_description', '');

document::$snippets['head_tags']['opengraph'] = '<meta property="og:url" content="'.document::href_ilink('').'" />'.PHP_EOL
        .'<meta property="og:type" content="website" />'.PHP_EOL
        .'<meta property="og:image" content="'.document::href_link(WS_DIR_IMAGES.'logotype.png').'" />';

include vmod::check(FS_DIR_HTTP_ROOT.WS_DIR_INCLUDES.'column_left.inc.php');

cart::reset();

$page = new view();
echo '<h1>'.language::translate('pm_payplug:thanks_for_your_order', 'Merci pour votre commande !', 'fr').'</h1>';
echo '<h4>'.language::translate('pm_payplug:confirmation_email', 'Un courriel de confirmation devrait vous être envoyé dans les minutes qui suivent.', 'fr').'</h4>';
echo $page->stitch('views/index');
