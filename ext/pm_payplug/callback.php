<?php

define('REQUIRE_POST_TOKEN', false); // Allow unsigned external incoming POST data
require_once('../../includes/app_header.inc.php');

// Pass the call to the payment module's method callback()
$payment = new mod_payment();
$payment->run('callback', 'pm_payplug');