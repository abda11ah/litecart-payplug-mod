### Module de paiement PayPlug pour Litecart ###

* Version : 1.1
* Licence : Creative Commons (CC BY-NC-SA 4.0) (http://creativecommons.org/licenses/by-nc-sa/4.0/)

### Installation ###

Avant l'installation, assurez-vous d'avoir au préalable ouvert un compte chez PayPlug (opération gratuite et instantanée)

* Installez tout d'abord le pack langue française pour Litecart.

Je ne vous cache pas que Litecart a des problèmes avec les caractères spéciaux sous Windows parce que Windows gère mal l'encodage UTF-8. Mais heureusement la plupart des hébergeurs de sites fonctionnent sous Linux.

* Copiez ensuite tous les fichiers du module dans le répertoire racine de Litecart.
* Connectez-vous à http://www.votresiteweb.com/admin
* Vérifiez que Litecart soit configuré pour utiliser la devise EURO.
(Attention le module Payplug ne fonctionne pas avec les autres devises !)
* Assurez-vous que votre devise EURO ait bien pour valeur 1 sinon vous risquez d'avoir de mauvaises surprises avec l'affichage des prix TTC.
* Entrez dans le menu [Modules] puis [Paiement]
* Sélectionnez PayPlug et cliquez sur [Installation]
* Renseignez les champs [Adresse e-mail] et [Mot de passe] et faites correspondre les statuts [commande payée] et [commande remboursée] par vos statuts de commande.
(Les statuts de commande doivent être créés dans la rubrique [Commandes] puis [Statuts des commandes])

Si lorsque vous arrivez sur la page de paiement vous voyez apparaître le message :
"Aucune transaction ne correspond à votre requête."
Eh bien cela signifie que votre compte Payplug n'est pas encore activé à 100%. Contactez Payplug pour résoudre le problème.

Notez que le module ne fonctionnera pas correctement en mode local (http://localhost ou http://127.0.0.1) parce qu'il ne pourra pas recevoir de confirmation de paiement, de remboursement ou autres informations de connexion depuis le serveur Payplug.

La méthode idéale pour le tester en local est d'utiliser son adresse IP publique en redirigeant le port TCP 80 de votre routeur ou box.

Si tout fonctionne comme sur des roulettes, je vous invite à faire un don pour aider au développement de modules meilleurs :
[http://wherbmaster.tumblr.com/post/114032848126/module-de-paiement-payplug-pour-litecart](http://wherbmaster.tumblr.com/post/114032848126/module-de-paiement-payplug-pour-litecart)

### À propos de PayPlug ###

PayPlug est un système de paiement qui fonctionne sans contrat de vente à distance (VAD).

Il n'y a pas de frais fixes ou mensuels.

Le client n'a pas besoin de compte pour payer.

Vous ne payez que s'il y a des ventes (2,5 %HT ou 3 %TTC par transaction) et ce taux est dégressif si le volume des transactions dépasse 2500€ / mois.

Ouvrir un compte chez PayPlug est quasi instantané mais vous devez au préalable avoir un compte en banque pour réceptionner les fonds.

PayPlug transfère ensuite les fonds vers votre banque par virement européen (SEPA).

Les pays supportés par le système sont, pour l'instant, tous les pays supportant la devise EURO et les virements bancaires de type SEPA.

----------------------------------------------------------------

### PayPlug payment module for Litecart ###

* Version : 1.0
* License : Creative Commons (CC BY-NC-SA 4.0) (http://creativecommons.org/licenses/by-nc-sa/4.0/)

### Installation ###



### About PayPlug ###