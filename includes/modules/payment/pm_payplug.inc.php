<?php

class pm_payplug {

    private $extensions = array('curl' => 'cURL', 'openssl' => 'OpenSSL');
    private $functions = array('base64_decode', 'base64_encode', 'json_decode', 'json_encode', 'urlencode');
    private $phpMin = '5.2.0';
    private $errCode = 200;
    private $system, $errMsg;
    public $id = __CLASS__;
    public $name = 'PayPlug France';
    public $description = '';
    public $configUrl = 'https://www.payplug.fr/portal/ecommerce/autoconfig';
    public $sandboxUrl = 'https://www.payplug.fr/portal/test/ecommerce/autoconfig';
    public $author = 'abda11ah <abdallah@gmx.com>';
    public $version = '1.1';
    public $website = 'https://www.payplug.fr/';
    public $priority = 0;

    private function init() {

        // Checks that all required extensions have been loaded
        foreach ($this->extensions as $name => $title) {
            if (!extension_loaded($name)) {
                $this->errCode = 500;
                $this->errMsg = language::translate(__CLASS__.':error_extension_needed', "La classe Payplug requiert l'extension [$title].", 'fr');
            }
        }
        // Checks that all required functions exist
        foreach ($this->functions as $func) {
            if (!function_exists($func)) {
                $this->errCode = 501;
                $this->errMsg = language::translate(__CLASS__.':error_function_needed', "La classe Payplug requiert la fonction [$func].", 'fr');
            }
        }
        // If the PHP version is strictly lower than 5.2.0
        if (version_compare(phpversion(), $this->phpMin, '<')) {
            $this->errCode = 503;
            $this->errMsg = language::translate(__CLASS__.':error_php_version_needed', "La classe Payplug requiert PHP [$this->phpMin] ou plus récent.", 'fr');
        }
        return ($this->errCode == 200 ? true : false);
    }

    public function install() {
        $auth = $this->serverAuth(filter_input(INPUT_POST, 'email'), filter_input(INPUT_POST, 'password'), filter_input(INPUT_POST, 'sandbox_mode'));
        if ($auth !== false) {
            $dir = FS_DIR_HTTP_ROOT.WS_DIR_DATA;
            file_put_contents("{$dir}/{$this->id}_auth.json", $auth);
        }
    }

    public function update() {
        $auth = $this->serverAuth(filter_input(INPUT_POST, 'email'), filter_input(INPUT_POST, 'password'), filter_input(INPUT_POST, 'sandbox_mode'));
        $dir = FS_DIR_HTTP_ROOT.WS_DIR_DATA;
        $authFile = "{$dir}/{$this->id}_auth.json";
        if (file_exists($authFile)) {
            unlink($authFile);
        }
        if ($auth !== false) {
            file_put_contents($authFile, $auth);
        }
    }

    public function uninstall() {
        $dir = FS_DIR_HTTP_ROOT.WS_DIR_DATA;
        $authFile = "{$dir}/{$this->id}_auth.json";
        if (file_exists($authFile)) {
            unlink($authFile);
        }
    }

    /*
     * Return selectable payment options for checkout
     */

    public function options($items, $subtotal, $tax, $currency_code, $customer) {

        // If not enabled
        if (empty($this->settings['status'])) {
            return;
        }

        // If not in geo zone
        if (!empty($this->settings['geo_zone_id'])) {
            if (!functions::reference_in_geo_zone($this->settings['geo_zone_id'], $customer['country_code'], $customer['zone_code']))
                return;
        }

        // If init error
        if ($this->init() == false) {
            notices::add('errors', language::translate(__CLASS__.':error_'.$this->errCode, 'Erreur&nbsp;', 'fr')."[$this->errCode] : $this->errMsg");
            return;
        }

        // If authentication file does not exists, try to create it by a new authentication request
        if (!$this->getAuthString()) {
            $auth = $this->serverAuth($this->settings['email'], $this->settings['password'], $this->settings['sandbox_mode']);
            if ($auth !== false) {
                file_put_contents("{$dir}/{$this->id}_auth.json", $auth);
            } else {
                notices::add('errors', language::translate(__CLASS__.':error_'.$this->errCode, 'Erreur&nbsp;', 'fr')."[$this->errCode] : $this->errMsg");
                return;
            }
        }

        $method = array(
            'title' => $this->name,
            'options' => array(
                array(
                    'id' => 'card',
                    'icon' => $this->settings['icon'],
                    'name' => language::translate(__CLASS__.':title_card_payment', 'Paiment par carte bancaire', 'fr'),
                    'description' => language::translate(__CLASS__.':description', 'Transactions faciles et sécurisées via PayPlug.', 'fr'),
                    'fields' => '',
                    'cost' => 0,
                    'tax_class_id' => 0,
                    'confirm' => language::translate(__CLASS__.':title_payment_confirmation', 'Confirmer le paiement', 'fr')
                )
            )
        );
        return $method;
    }

    public function transfer($order) {

        // If another currency than EURO was selected
        if ($order->data['currency_code'] != 'EUR') {
            return array(
                'error' => language::translate(__CLASS__.':error_currency', 'Erreur : PayPlug ne supporte pas les paiements dans une autre devise que l\'EURO.', 'fr')
            );
        }

        // Generate payment URL
        $paymentDue = $this->rawFormat($order->data['payment_due'], $order->data['currency_code'], $order->data['currency_value']);
        $paymentUrl = $this->generateUrl(array(
            'amount' => str_replace('.', '', $paymentDue),
            'currency' => !empty($this->settings['use_store_currency']) ? settings::get('store_currency_code') : $order->data['currency_code'],
            'ipnUrl' => document::link(WS_DIR_EXT."/{$this->id}/callback.php"),
            'cancelUrl' => document::ilink('checkout'),
            'returnUrl' => document::ilink('payplug_success'),
            'email' => $order->data['customer']['email'],
            'firstName' => $order->data['customer']['firstname'],
            'lastName' => $order->data['customer']['lastname'],
            'customer' => $order->data['customer']['id'],
            'order' => $order->data['id'],
            'customData' => $order->data['uid'],
            'origin' => settings::get('store_name', '')
        ));

        // Save order
        $order->save();
        /*
          if ($this->settings['popup_payment_page'] == '1') {
          $html = '<script type="text/javascript" src="http://www.payplug.fr/static/button/scripts/payplug.js"></script>'.PHP_EOL;
          $html .= '<script type="text/javascript">Payplug.showPayment(\''.$paymentUrl.'\');</script>';
          return array(
          'method' => 'html',
          'content' => $html
          );
          }
         */
        return array(
            'action' => $paymentUrl,
            'method' => 'get',
        );
    }

    public function callback() {

        if (!function_exists('getallheaders')) {

            function getallheaders() {
                $headers = array();
                foreach ($GLOBALS['server'] as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                        $headers[$name] = $value;
                    } else if ($name == 'CONTENT_TYPE') {
                        $headers['Content-Type'] = $value;
                    } else if ($name == 'CONTENT_LENGTH') {
                        $headers['Content-Length'] = $value;
                    } else {
                        $headers[$name] = $value;
                    }
                }
                return $headers;
            }

        }

        /* For more security, put the keys in uppercase and retrieve
         * the signature using the key in uppercase
         */
        $headers = array_change_key_case(getallheaders(), CASE_UPPER);
        $signature = base64_decode($headers['PAYPLUG-SIGNATURE']);

        /* The data is sent in the body of the POST request in JSON format
         * (MIME type = application / json).
         * Example : {"state": "paid", "customer", "2", "transaction_id": 4121, "custom_data": "29", "order": "42"}
         */
        $body = file_get_contents('php://input');
        $data = json_decode($body, true);

        // Make sure we have an order ID
        if (empty($data['custom_data'])) {
            header('HTTP/1.1 400 Bad Request');
            die('Bad Request');
        }

        // Validate the given public key
        $auth = $this->getAuthString();
        $publicKey = openssl_pkey_get_public($auth['payplugPublicKey']);
        $isSignatureValid = openssl_verify($body, $signature, $publicKey, OPENSSL_ALGO_SHA1);

        if ($isSignatureValid !== 0) {
            $o = $this->getOrderByUID($data['custom_data']);
            if ($o === false) {
                header('HTTP/1.1 400 Bad Request');
                die('Bad Request');
            }
            $order = new ctrl_order('load', $o['id']);
            switch ($data['state']) {
                case 'paid':
                    $order->data['order_status_id'] = $this->settings['order_status_paid'];
                    // Send e-mail to customer
                    $order->email_order_copy($data['email']);
                    break;
                case 'refunded':
                    $order->data['order_status_id'] = $this->settings['order_status_refunded'];
                    break;
            }
            $order->save();
        } else {
            header('HTTP/1.1 403 Forbidden');
            die('Access Denied');
        }
    }

    public function settings() {
        return array(
            array(
                'key' => 'status',
                'default_value' => '1',
                'title' => language::translate(__CLASS__.':title_status', 'Statut', 'fr'),
                'description' => language::translate(__CLASS__.':description_status', 'Activer ou désactiver le module.', 'fr'),
                'function' => 'toggle("e/d")'
            ),
            array(
                'key' => 'icon',
                'default_value' => 'images/payment/payplug.png',
                'title' => language::translate(__CLASS__.':title_icon', 'Icône', 'fr'),
                'description' => language::translate(__CLASS__.':description_icon', 'Adresse Web de l\'icône à afficher.', 'fr'),
                'function' => 'input()'
            ),
            array(
                'key' => 'email',
                'default_value' => '',
                'title' => language::translate(__CLASS__.':title_email', 'Adresse e-mail', 'fr'),
                'description' => language::translate(__CLASS__.':description_email', 'Votre adresse e-mail chez PayPlug.', 'fr'),
                'function' => 'input()'
            ),
            array(
                'key' => 'password',
                'default_value' => '',
                'title' => language::translate(__CLASS__.':title_password', 'Mot de passe', 'fr'),
                'description' => language::translate(__CLASS__.':description_password', 'Votre mot de passe chez PayPlug.', 'fr'),
                'function' => 'input()'
            ),
            array(
                'key' => 'sandbox_mode',
                'default_value' => '0',
                'title' => language::translate(__CLASS__.':title_sandbox_mode', 'Mode TEST ou Sandbox', 'fr'),
                'description' => language::translate(__CLASS__.':description_sandbox_mode', 'Mode simulation dans lequel la carte bancaire n\'est pas débitée.', 'fr'),
                'function' => 'toggle("y/n")'
            ), /*
              array(
              'key' => 'popup_payment_page',
              'default_value' => '0',
              'title' => \language::translate(__CLASS__.':title_popup_payment', 'Page de paiement pop-up', 'fr'),
              'description' => \language::translate(__CLASS__.':description_popup_payment', 'Choisissez d\'afficher la page de paiement sous forme de fenêtre pop-up ou de rediriger le client vers le site PayPlug.', 'fr'),
              'function' => 'toggle("y/n")',
              ), */
            array(
                'key' => 'use_store_currency',
                'default_value' => '1',
                'title' => language::translate(__CLASS__.':title_use_store_currency', 'Utiliser la devise de la boutique', 'fr'),
                'description' => language::translate(__CLASS__.':description_force_store_currency', 'Utiliser la devise de la boutique pour toutes les transactions.', 'fr'),
                'function' => 'toggle("y/n")'
            ),
            array(
                'key' => 'order_status_paid',
                'default_value' => '0',
                'title' => language::translate(__CLASS__.':title_order_status_paid', 'Statut de la commande payée', 'fr').': '.language::translate(__CLASS__.':title_paid', 'Payée', 'fr'),
                'description' => language::translate(__CLASS__.':description_order_status_paid', 'Choisissez le statut qui sera utilisé en cas de commande payée.', 'fr'),
                'function' => 'order_status()'
            ),
            array(
                'key' => 'order_status_refunded',
                'default_value' => '0',
                'title' => language::translate(__CLASS__.':title_order_status_refunded', 'Statut de la commande remboursée', 'fr').': '.language::translate(__CLASS__.':title_refunded', 'Remboursée', 'fr'),
                'description' => language::translate(__CLASS__.':description_order_status_refunded', 'Choisissez le statut qui sera utilisé en cas de commande remboursée.', 'fr'),
                'function' => 'order_status()'
            ),
            array(
                'key' => 'geo_zone_id',
                'default_value' => '',
                'title' => language::translate(__CLASS__.':title_geo_zone_limitation', 'Limitation géographique', 'fr'),
                'description' => language::translate(__CLASS__.':description_geo_zone', 'Limiter le module à une certaine zone géographique. Sinon laisser le champ vide.', 'fr'),
                'function' => 'geo_zones()'
            ),
            array(
                'key' => 'priority',
                'default_value' => '0',
                'title' => language::translate(__CLASS__.':title_priority', 'Priorité', 'fr'),
                'description' => language::translate(__CLASS__.':description_priority', 'Priorité dans laquelle ce module sera traité.', 'fr'),
                'function' => 'int()'
            )
        );
    }

    private function serverAuth($email, $password, $sandbox = false) {

        $process = curl_init($sandbox == true ? $this->sandboxUrl : $this->configUrl);

        curl_setopt($process, CURLOPT_HEADER, true);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($process, CURLOPT_SSLVERSION, 4);
        curl_setopt($process, CURLOPT_USERPWD, $email.':'.$password);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($process, CURLOPT_CAINFO, FS_DIR_HTTP_ROOT.WS_DIR_MODULES.'payment/cacert.pem');

        $answer = curl_exec($process);
        $headerSize = curl_getinfo($process, CURLINFO_HEADER_SIZE);
        // HTTP response code (200, 401, 404...)
        $httpCode = curl_getinfo($process, CURLINFO_HTTP_CODE);

        // Extracts JSON
        $body = substr($answer, $headerSize);
        // Extracts headers and splits the string containing the headers into lines in an array
        $headers = explode("\r\n", substr($answer, 0, $headerSize));

        /*
         * Splits the first line (containing HTTP response details) by spaces
         * for the 2 first matches. The last one will contains the remaining
         * text which is equivalent to the readable HTTP response message.
         */
        $httpResponse = explode(' ', $headers[0], 3);
        // The well deserved message
        $httpMsg = (isset($httpResponse[2]) ? $httpResponse[2] : '');
        // Curl error code (different from the HTTP response code)
        $curlErrNo = curl_errno($process);
        // Human readable message
        $curlErrMsg = curl_error($process);
        curl_close($process);

        // If there was no error
        if ($curlErrNo == 0) {
            $body = json_decode($body);

            // Authentication OK
            if ($httpCode == 200) {
                $auth = json_encode(array(
                    'currencies' => $body->currencies,
                    'amount_max' => $body->amount_max,
                    'amount_min' => $body->amount_min,
                    'url' => $body->url,
                    'payplugPublicKey' => $body->payplugPublicKey,
                    'yourPrivateKey' => $body->yourPrivateKey
                ));
                return $auth;
            }
            // Wrong email and/or password
            elseif ($httpCode == 401) {
                $this->errCode = 401;
                $this->errMsg = language::translate(__CLASS__.':error_invalid_credentials', 'Nom d\'utilisateur ou mot de passe Payplug non-valide', 'fr');
            }
            // Access Forbidden if account is not activated
            elseif ($httpCode == 403) {
                $this->errCode = 403;
                $this->errMsg = language::translate(__CLASS__.':error_access_forbidden', 'Accès refusé : Veuillez vérifier que votre compte Payplug est bien activé', 'fr');
            }
            // I wonder what this could be
            else {
                $this->errCode = $httpCode;
                $this->errMsg = $httpMsg;
            }
        } else {
            $this->errCode = $curlErrNo;
            $this->errMsg = $curlErrMsg;
        }
        return false;
    }

    /**
     * The method which actually generates the URL.
     */
    private function generateUrl($params) {
        /* Generation of the <data> parameter */
        $remap_params = array(
            /* our key => payplug key */
            'amount' => 'amount',
            'cancelUrl' => 'cancel_url',
            'currency' => 'currency',
            'customData' => 'custom_data',
            'customer' => 'customer',
            'email' => 'email',
            'firstName' => 'first_name',
            'ipnUrl' => 'ipn_url',
            'lastName' => 'last_name',
            'order' => 'order',
            'origin' => 'origin',
            'returnUrl' => 'return_url'
        );
        $payment_params = array();

        /* Remaps $params keys to the one expected by Payplug payment page
         * That is, transform array('amount'=>100,'firstName'=>'bob')
         * to                 array('amount'=>100,'first_name'=>'bob')
         */
        foreach ($remap_params as $our_key => $payplug_key) {
            if (isset($params[$our_key])) {
                $payment_params[$payplug_key] = $params[$our_key];
            }
            if ($our_key == 'origin') {
                $payment_params[$payplug_key] = (isset($params[$our_key]) ? $params[$our_key] : "")." payplug-php ".$this->version.' PHP '.phpversion();
            }
        }

        $url_params = http_build_query($payment_params);
        $data = urlencode(base64_encode($url_params));

        /* Generation of the <signature> parameter */
        $auth = $this->getAuthString();
        $privateKey = openssl_pkey_get_private($auth['yourPrivateKey']);
        openssl_sign($url_params, $signature, $privateKey, OPENSSL_ALGO_SHA1);
        $signature = urlencode(base64_encode($signature));

        return $auth['url'].'?data='.$data.'&sign='.$signature;
    }

    private function rawFormat($value, $currency_code, $currency_value) {
        if (!empty($this->settings['use_store_currency'])) {
            $currency_code = settings::get('store_currency_code');
            $currency_value = 1;
        }
        return number_format($value * $currency_value, currency::$currencies[$currency_code]['decimals'], '.', '');
    }

    private function getOrderByUID($param) {
        $q = database::query("select `id` from ".DB_TABLE_ORDERS." where `uid` = '".$param."' limit 1;");
        return (database::num_rows($q) ? database::fetch($q) : false);
    }

    private function getAuthString() {
        $dir = FS_DIR_HTTP_ROOT.WS_DIR_DATA;
        $authFile = "{$dir}/{$this->id}_auth.json";
        if (file_exists($authFile)) {
            return json_decode(file_get_contents($authFile), true);
        }
        return false;
    }

}